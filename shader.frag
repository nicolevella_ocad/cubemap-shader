#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec3 u_camera;
uniform vec3 u_light;
uniform vec2 u_resolution;

varying vec4 v_position;

#ifdef MODEL_VERTEX_COLOR
varying vec4 v_color;
#endif

#ifdef MODEL_VERTEX_NORMAL
varying vec3 v_normal;
#endif

#ifdef MODEL_VERTEX_TANGENT
varying vec4 v_tangent;
varying mat3 v_tangentToWorld;
#endif

#ifdef MODEL_VERTEX_TEXCOORDS
varying vec2 v_texcoord;
#endif

#ifndef FNC_TONEMAP
#define FNC_TONEMAP
vec3 tonemap(const vec3 x) { 
    return x / (x + vec3(1.0));
}
#endif

#ifndef FNC_ENVMAP
#define FNC_ENVMAP
uniform samplerCube u_cubeMap;
const float numMips = 1.0;
vec3 envMap(vec3 _normal, float _roughness) {
    vec4 color = mix(   textureCube( u_cubeMap, _normal, (_roughness * numMips) ), 
                        textureCube( u_cubeMap, _normal, min((_roughness * numMips) + 1.0, numMips)), 
                        fract(_roughness * numMips) );
    return tonemap(color.rgb);
}
#endif

#define PI 3.141592653589793
#define TWO_PI 6.283185307179586
#define CIRCLE(st,pos,size)smoothstep(0.,1500./u_resolution.y,size-length(pos-st))
#define BUTTER PI/TWO_PI/PI

//	Simplex 3D Noise 
//	by Ian McEwan, Ashima Arts
//
vec4 permute(vec4 x){return mod(((x*34.0)+1.0)*x, 289.0);}
vec4 taylorInvSqrt(vec4 r){return 1.79284291400159 - 0.85373472095314 * r;}

float snoise(vec3 v){ 
  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
  const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

// First corner
  vec3 i  = floor(v + dot(v, C.yyy) );
  vec3 x0 =   v - i + dot(i, C.xxx) ;

// Other corners
  vec3 g = step(x0.yzx, x0.xyz);
  vec3 l = 1.0 - g;
  vec3 i1 = min( g.xyz, l.zxy );
  vec3 i2 = max( g.xyz, l.zxy );

  //  x0 = x0 - 0. + 0.0 * C 
  vec3 x1 = x0 - i1 + 1.0 * C.xxx;
  vec3 x2 = x0 - i2 + 2.0 * C.xxx;
  vec3 x3 = x0 - 1. + 3.0 * C.xxx;

// Permutations
  i = mod(i, 289.0 ); 
  vec4 p = permute( permute( permute( 
             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
           + i.y + vec4(0.0, i1.y, i2.y, 1.0 )) 
           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

// Gradients
// ( N*N points uniformly over a square, mapped onto an octahedron.)
  float n_ = 1.0/7.0; // N=7
  vec3  ns = n_ * D.wyz - D.xzx;

  vec4 j = p - 49.0 * floor(p * ns.z *ns.z);  //  mod(p,N*N)

  vec4 x_ = floor(j * ns.z);
  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

  vec4 x = x_ *ns.x + ns.yyyy;
  vec4 y = y_ *ns.x + ns.yyyy;
  vec4 h = 1.0 - abs(x) - abs(y);

  vec4 b0 = vec4( x.xy, y.xy );
  vec4 b1 = vec4( x.zw, y.zw );

  vec4 s0 = floor(b0)*2.0 + 1.0;
  vec4 s1 = floor(b1)*2.0 + 1.0;
  vec4 sh = -step(h, vec4(0.0));

  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
  vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

  vec3 p0 = vec3(a0.xy,h.x);
  vec3 p1 = vec3(a0.zw,h.y);
  vec3 p2 = vec3(a1.xy,h.z);
  vec3 p3 = vec3(a1.zw,h.w);

//Normalise gradients
  vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;

// Mix final noise value
  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
  m = m * m;
  return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), 
                                dot(p2,x2), dot(p3,x3) ) );
}


void main(void) {
    vec3 color = vec3(1.);
    vec3 uv = v_position.xyz;

    // our normals, lights, and camera need to be normalized
    vec3 n = v_normal*.5+.5;
    vec3 l = u_light*.5+.5;
    vec3 v = (u_camera - v_position.xyz)*.5+.5;

    // diffuse the light
    float diffuse = (dot(n, l) + 1.0 ) * 0.001;
    color *= diffuse;

    // distort the normals by noise and time
    n-=snoise(uv*3.+vec3(u_time*BUTTER,u_time*BUTTER,u_time*BUTTER))*.5+.5;

    // our reflections
    vec3 r = reflect(-v, n);

    // lets distort our reflections based on a larger noisefield
    // to reallt get that warped reflection look
    r-=snoise(uv*6.+vec3(u_time*BUTTER,u_time*BUTTER,u_time*BUTTER))*.5+.5;

    // envMap takes two arguments, one for the object normals and one for the roughness
    vec3 specular = envMap(r, smoothstep(1.,100., v_position.z) );

    // amp the brightness
    color += specular*10.;

    gl_FragColor = vec4(color, 1.0);
}
