# Cubemap Shader

A glsl shader that uses cubemaps

I progressed on my gold shader to include cubemaps and reflections. This was a bit challenging for me. I used GlslViewer to code and compile/render. Some notes:

– I had to use 3D noise instead of 2D noise. This allowed for the flow of the noise to go around the corner of the cube. When using 3D shapes for this shader, up until now I had been using just 2D noise on each face, sort of “cheating”. Now it is a proper flowing 3D noise cube.

– I took away the gold colouring so that only the reflections remained, this caused me to have to really amp the specular value so that you could actually see the reflections

– I would like it if each face of the cube, the noise animated not in any direction but just on the face itself. it’s hard for me to describe. on the top and bottom faces, the noise animates exactly how I want, but on the other sides it sort of “flows” toward an edge. i’d rather it not do this but I couldn’t figure out how

to run this shader, you need glslViewer installed on your machine and then just run this command from within the shaders file folder

 `glslviewer shader.frag cube.obj -C cubemap.png` 
